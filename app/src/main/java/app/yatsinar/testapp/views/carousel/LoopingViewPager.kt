package app.yatsinar.testapp.views.carousel

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet

/**
 * Created by Roman Iatcyna on 16.02.17.

 * ViewPager that loops displayed items.
 */
class LoopingViewPager : ViewPager {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}


    override fun setAdapter(adapter: PagerAdapter) {
        throw UnsupportedOperationException("Use setAdapter(LoopingPagerAdapter) method!")
    }

    fun setAdapter(pagerAdapter: LoopingPagerAdapter<*, *>?) {
        super.setAdapter(pagerAdapter)
    }

    override fun getAdapter(): LoopingPagerAdapter<*, *>? {
        return super.getAdapter() as LoopingPagerAdapter<*, *>
    }

    override fun addOnPageChangeListener(listener: OnPageChangeListener) {
        super.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(loopPosition: Int, positionOffset: Float, positionOffsetPixels: Int) {
                adapter?.let {
                    listener.onPageScrolled(
                            it.getRealPosition(loopPosition), positionOffset, positionOffsetPixels)
                }
            }

            override fun onPageSelected(loopPosition: Int) {
                adapter?.let {
                    listener.onPageSelected(it.getRealPosition(loopPosition))
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                listener.onPageScrollStateChanged(state)
            }
        })
    }

    override fun setCurrentItem(item: Int) {
        setCurrentItem(item, false)
    }

    override fun setCurrentItem(item: Int, smoothScroll: Boolean) {
        val loopPosition: Int
        if (smoothScroll) {
            loopPosition = adapter!!.getNearestLoopPosition(currentItem, item)
        } else {
            loopPosition = adapter!!.getInitialLoopPosition(item)
        }
        super.setCurrentItem(loopPosition, smoothScroll)
    }

}
