package app.yatsinar.testapp.views

import android.content.Context
import android.text.Editable
import android.util.AttributeSet
import android.widget.EditText
import com.mcxiaoke.koi.ext.KoiTextWatcher

/**
 * Custom EditText wrapper, contains additional methods.
 */
class EditText : EditText {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    /**
     * @param callback is only invoked if this view has focus
     */
    fun listenToFocusedTextChanges(callback: (String?) -> Unit) {
        addTextChangedListener(object : KoiTextWatcher() {
            override fun afterTextChanged(s: Editable) {
                if (hasFocus()) {
                    callback.invoke(s.toString())
                }
            }
        })
    }
}
