package app.yatsinar.testapp.views.carousel;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import app.yatsinar.testapp.R;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by Roman Iatcyna on 16.02.17.
 * Indicators (circles) view for ViewPager
 */
public class ViewPagerIndicatorView
        extends View
        implements ViewPager.OnPageChangeListener {

    @ColorRes
    public static final int DEFAULT_NORMAL_ITEM_COLOR = android.R.color.white;
    @ColorRes
    public static final int DEFAULT_SELECTED_ITEM_COLOR = android.R.color.black;
    @DimenRes
    public static final int DEFAULT_INDICATOR_INTERVAL = R.dimen.hero_carousel_indicator_default_interval;

    @NonNull
    private final Paint normalItemPaint = new Paint(ANTI_ALIAS_FLAG);
    @NonNull
    private final Paint selectedItemPaint = new Paint(ANTI_ALIAS_FLAG);
    @Nullable
    protected ViewPager viewPager;
    protected int indicatorsInterval;

    protected boolean isRegisteredAdapterDataSetObserver;
    protected DataSetObserver adapterDataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            if (viewPager != null
                    && viewPager.getAdapter() != null
                    && viewPager.getAdapter().getCount() > 0
                    && isRegisteredAdapterDataSetObserver) {
                isRegisteredAdapterDataSetObserver = false;
                viewPager.getAdapter().unregisterDataSetObserver(adapterDataSetObserver);
            }
            invalidate();
        }
    };

    public ViewPagerIndicatorView(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public ViewPagerIndicatorView(@NonNull Context context, @NonNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public void initView(@NonNull Context context, @NonNull AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(
                attrs,
                R.styleable.ViewPagerIndicatorView,
                0,
                0
        );
        try {
            indicatorsInterval = attributes.getDimensionPixelOffset(
                    R.styleable.ViewPagerIndicatorView_indicators_interval,
                    context.getResources().getDimensionPixelOffset(DEFAULT_INDICATOR_INTERVAL)
            );
            @ColorInt int normalItemColor = attributes.getColor(
                    R.styleable.ViewPagerIndicatorView_normal_item_color,
                    ContextCompat.getColor(context, DEFAULT_NORMAL_ITEM_COLOR)
            );
            @ColorInt int selectedItemColor = attributes.getColor(
                    R.styleable.ViewPagerIndicatorView_selected_item_color,
                    ContextCompat.getColor(context, DEFAULT_SELECTED_ITEM_COLOR)
            );
            normalItemPaint.setColor(normalItemColor);
            normalItemPaint.setAlpha(Color.alpha(normalItemColor));
            selectedItemPaint.setColor(selectedItemColor);
            selectedItemPaint.setAlpha(Color.alpha(selectedItemColor));
        } finally {
            attributes.recycle();
        }
    }

    public void setViewPager(@NonNull ViewPager viewPager) {
        this.viewPager = viewPager;
        viewPager.addOnPageChangeListener(this);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawIndicators(canvas);
    }

    private void drawIndicators(Canvas canvas) {
        //fills background
        canvas.drawColor(
                ContextCompat.getColor(
                        getContext(),
                        android.R.color.transparent
                )
        );

        //draws circles
        if (viewPager != null
                && viewPager.getAdapter() != null
                && viewPager.getAdapter() instanceof LoopingPagerAdapter) {
            LoopingPagerAdapter adapter = (LoopingPagerAdapter) viewPager.getAdapter();

            if (adapter.getRealCount() == 0 && !isRegisteredAdapterDataSetObserver) {
                isRegisteredAdapterDataSetObserver = true;
                adapter.registerDataSetObserver(adapterDataSetObserver);
            }
            int currentItem = adapter.getRealCount() != 0 ? viewPager.getCurrentItem() % adapter.getRealCount() : 0;
            int itemsCount = adapter.getRealCount();
            int circleRadius = getHeight() / 2;
            int indicatorsWidth = (itemsCount - 1) * indicatorsInterval + itemsCount * 2 * circleRadius;
            int indicatorsOffset = (getWidth() - indicatorsWidth) / 2;

            for (int i = 0; i < adapter.getRealCount(); i++) {
                Paint circlePaint;
                if (i != currentItem) {
                    circlePaint = normalItemPaint;
                } else {
                    circlePaint = selectedItemPaint;
                }
                canvas.drawCircle(
                        indicatorsOffset + circleRadius + (2 * circleRadius + indicatorsInterval) * i,
                        circleRadius,
                        circleRadius,
                        circlePaint
                );
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //isn't required action
    }

    @Override
    public void onPageSelected(int position) {
        invalidate();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //isn't required action
    }

}
