package app.yatsinar.testapp.views.carousel

import android.support.v4.view.PagerAdapter
import android.util.Log
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup

/**
 * Created by Roman Iatcyna on 16.02.17.

 * Common carousel adapter for View Pager.

 * Provides pseudo-infinite loop for ViewPagers items.
 * E.g. you have 8 items, and you want to scroll through them in loop.
 * This adapter provides LOOP_COUNT / 2 of items to the left and to the right of original items,
 * this gives an illusion of infinite looping through items.

 * LOOPS_COUNT mustn't be too large, since ViewPager might to measure some inner stuff inside it,
 * and large numbers may block the UI thread.
 *
 * @param isLooping if true - then traverse further the last item leads to first one and vice versa.

 */
abstract class LoopingPagerAdapter<T, V : LoopingPagerAdapter.ViewHolder>(private val isLooping: Boolean) : PagerAdapter() {

    private val viewHolders = SparseArray<V>()

    companion object {

        private val LOOPS_COUNT = 2000

    }
    var items: List<T> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    /**
     * @return real count of displayed items.
     */
    fun getRealCount() = items.size

    /**
     * @return if isLooping == false it returns real items count, otherwise returns the total value of items
     * * that equals to real items count multiplied by loops number.
     */
    override fun getCount(): Int {
        return if (isLooping) getRealCount() * LOOPS_COUNT else getRealCount()
    }

    /**
     * If you have 8 items and declare LOOPS_COUNT as 3,
     * than you actually have 24 displayed items.
     * Where 0-7, 8-15 and 16-23 items are the same.
     * Thus getRealPosition(0) == getRealPosition(8) == getRealPosition(16) == 0

     * @param loopPosition ViewPager's position inside the set of iteratively looped items.
     * *
     * @return corresponded real position.
     */
    fun getRealPosition(loopPosition: Int): Int {
        return loopPosition % getRealCount()
    }

    /**
     * If you have 8 items and declare LOOPS_COUNT as 3,
     * than you actually have 24 displayed items.

     * If you want to set 0 current item, you actually need to set
     * 8 position, to have 1 loop to left and 1 to the right of the current 8 of items.

     * @param initialRealPosition position in the items set.
     * *
     * @return loop position that has equals available loops to the left and to the right.
     */
    fun getInitialLoopPosition(initialRealPosition: Int): Int {
        return initialRealPosition + getRealCount() * LOOPS_COUNT / 2
    }

    /**
     * If you have 8 items and declare LOOPS_COUNT as 3,
     * than you actually have 24 displayed items.

     * If ViewPager is currently displaying 7th item on the 23 loop position,
     * and you want to slide to the 3rd, then you need closest loop position that corresponds to 3rd item position.

     * @param currentLoopPosition currently displayed by ViewPager position.
     * *
     * @param item position in the items which closest loop position we look up for.
     * *
     * @return loop position of item that closest to currentLoopPosition.
     */
    fun getNearestLoopPosition(currentLoopPosition: Int, item: Int): Int {
        return currentLoopPosition + (item - getRealPosition(currentLoopPosition))
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val viewHolder = instantiateInternal(container, if (isLooping) getRealPosition(position) else position)
        return viewHolder.view
    }

    private fun instantiateInternal(container: ViewGroup, position: Int): V {
        val item = items[position]
        val viewHolder = onCreateViewHolder(item, container)
        container.addView(viewHolder.view)
        viewHolders.put(position, viewHolder)
        return viewHolder
    }

    abstract fun onCreateViewHolder(data: T, parent: ViewGroup): V

    fun getViewHolder(position: Int): V? {
        return viewHolders.get(position)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any) = view == `object`

    abstract class ViewHolder(val view: View)

}
