package app.yatsinar.testapp.application

import android.app.Application

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
class TestApplication : Application() {

    private var applicationComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    fun getApplicationComponent(): ApplicationComponent {
        applicationComponent?.let { return it }
        throw IllegalStateException("Application Component is Null")
    }

}
