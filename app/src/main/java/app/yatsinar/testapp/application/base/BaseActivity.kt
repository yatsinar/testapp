package app.yatsinar.testapp.application.base

import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity

/**
 * Created by Roman Iatcyna on 16.02.17.
 */
abstract class BaseActivity<P : MvpPresenter<V>, V : MvpView> : AppCompatActivity(), MvpView {

    @JvmField
    var screenPresenter: P? = null

    open fun setScreenPresenter(screenPresenter: P) {
        this.screenPresenter = screenPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        createScreenComponent()
        if (screenPresenter != null) {
            screenPresenter!!.attachView(getView())
        } else {
            throw IllegalStateException("Application presenter is null.")
        }
    }

    abstract fun createScreenComponent()

    abstract fun getView() : V


    override fun onDestroy() {
        super.onDestroy()
        if (screenPresenter != null) {
            screenPresenter!!.detachView()
        }
    }

}
