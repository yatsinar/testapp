package app.yatsinar.testapp.application.base

import android.support.annotation.CallSuper
import java.lang.ref.WeakReference

/**
 * Created by Roman Iatcyna on 16.02.17.
 */
open abstract class BasePresenter<V : MvpView>
    : MvpPresenter<V> {

    private var viewRef = WeakReference<V>(null)

    protected val view: V?
        get() = viewRef.get()

    @CallSuper
    override fun attachView(view: V) {
        viewRef = WeakReference(view)
    }

    @CallSuper
    override fun detachView() {
        viewRef.clear()
    }

}