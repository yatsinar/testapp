package app.yatsinar.testapp.application

import android.util.Log

import javax.inject.Singleton

import app.yatsinar.testapp.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
@Module
class ApplicationModule(private val testApplication: TestApplication) {

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> Log.d("HTTP", message) }
                .setLevel(
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.HEADERS else HttpLoggingInterceptor.Level.NONE
                )
    }

    @Provides
    fun provideDefaultOkHttpClientBuilder(
            httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient.Builder {
        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
    }

    @Provides
    @Singleton
    fun provideRestAdapter(clientBuilder: OkHttpClient.Builder): Retrofit {
        return Retrofit.Builder()
                .client(clientBuilder.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
                .baseUrl("http://www.ecb.europa.eu/")
                .build()
    }

}
