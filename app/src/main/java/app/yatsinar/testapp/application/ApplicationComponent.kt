package app.yatsinar.testapp.application

import javax.inject.Singleton

import app.yatsinar.testapp.api.ApiModule
import app.yatsinar.testapp.screens.exchange.ExchangeScreenComponent
import app.yatsinar.testapp.screens.exchange.ExchangeScreenModule
import dagger.Component

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, ApiModule::class))
interface ApplicationComponent {

    fun getRatesActivityComponent(exchangeScreenModule: ExchangeScreenModule): ExchangeScreenComponent

}
