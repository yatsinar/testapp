package app.yatsinar.testapp.application.base

/**
 * Created by Roman Iatcyna on 16.02.17.
 */
interface MvpPresenter<in V : MvpView> {

    fun attachView(view : V)

    fun detachView()

}