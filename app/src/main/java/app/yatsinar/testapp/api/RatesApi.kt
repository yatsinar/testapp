package app.yatsinar.testapp.api

import app.yatsinar.testapp.api.models.RatesResponse
import retrofit2.http.GET
import rx.Observable

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
interface RatesApi {

    @GET("stats/eurofxref/eurofxref-daily.xml")
    fun rates(): Observable<RatesResponse>

}
