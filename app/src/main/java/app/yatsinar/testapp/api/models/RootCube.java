package app.yatsinar.testapp.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Element;

/**
 * Created by Roman Iatcyna on 13.02.17.
 */
public class RootCube implements Parcelable {

    @Element(name = "Cube")
    @NonNull
    private TimestampCube timestampCube;

    public RootCube(@Element(name = "Cube") @NonNull TimestampCube timestampCube) {
        this.timestampCube = timestampCube;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.timestampCube, flags);
    }

    protected RootCube(Parcel in) {
        this.timestampCube = in.readParcelable(TimestampCube.class.getClassLoader());
    }

    @NonNull
    public TimestampCube getTimestampCube() {
        return timestampCube;
    }

    public static final Creator<RootCube> CREATOR = new Creator<RootCube>() {
        @Override
        public RootCube createFromParcel(Parcel source) {
            return new RootCube(source);
        }

        @Override
        public RootCube[] newArray(int size) {
            return new RootCube[size];
        }
    };
}
