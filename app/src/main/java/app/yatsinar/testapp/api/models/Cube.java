package app.yatsinar.testapp.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Attribute;

/**
 * Created by Roman Iatcyna on 13.02.17.
 */
public class Cube implements Parcelable {

    public static final Creator<Cube> CREATOR = new Creator<Cube>() {
        @Override
        public Cube createFromParcel(Parcel source) {
            return new Cube(source);
        }

        @Override
        public Cube[] newArray(int size) {
            return new Cube[size];
        }
    };

    @Attribute(name = "currency")
    @NonNull
    private CurrencyType currency;

    @Attribute(name = "rate")
    @NonNull
    private String rate;

    public Cube(@Attribute(name = "currency") @NonNull CurrencyType currency,
                @Attribute(name = "rate") @NonNull String rate) {
        this.currency = currency;
        this.rate = rate;
    }

    protected Cube(Parcel in) {
        this.currency = CurrencyType.values()[in.readInt()];
        this.rate = in.readString();
    }

    @NonNull
    public CurrencyType getCurrency() {
        return currency;
    }

    @NonNull
    public String getRate() {
        return rate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.currency.ordinal());
        dest.writeString(this.rate);
    }

}

