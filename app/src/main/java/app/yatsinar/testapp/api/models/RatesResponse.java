package app.yatsinar.testapp.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
@Root(name = "gesmes:Envelope")
public class RatesResponse implements Parcelable {


    public static final Creator<RatesResponse> CREATOR = new Creator<RatesResponse>() {
        @Override
        public RatesResponse createFromParcel(Parcel source) {
            return new RatesResponse(source);
        }

        @Override
        public RatesResponse[] newArray(int size) {
            return new RatesResponse[size];
        }
    };

    @Element(name = "Cube")
    @NonNull
    private RootCube rootCube;


    public RatesResponse(@Element(name = "Cube") @NonNull RootCube rootCube) {
        this.rootCube = rootCube;
    }

    protected RatesResponse(Parcel in) {
        this.rootCube = in.readParcelable(RootCube.class.getClassLoader());
    }

    @NonNull
    public RootCube getRootCube() {
        return rootCube;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.rootCube, flags);
    }

}
