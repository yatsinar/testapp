package app.yatsinar.testapp.api

import android.content.Context

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
@Module
class ApiModule {

    @Provides
    fun provideRestApi(retrofit: Retrofit): RatesApi {
        return retrofit.create<RatesApi>(RatesApi::class.java)
    }

}
