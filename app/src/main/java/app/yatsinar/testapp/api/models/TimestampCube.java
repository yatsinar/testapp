package app.yatsinar.testapp.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by Roman Iatcyna on 13.02.17.
 */
public class TimestampCube implements Parcelable {

    public static final Creator<TimestampCube> CREATOR = new Creator<TimestampCube>() {
        @Override
        public TimestampCube createFromParcel(Parcel source) {
            return new TimestampCube(source);
        }

        @Override
        public TimestampCube[] newArray(int size) {
            return new TimestampCube[size];
        }
    };

    @Attribute(name = "time")
    @NonNull
    private String time;

    @ElementList(inline = true, entry = "Cube")
    @NonNull
    private List<Cube> cubes;

    public TimestampCube(@Attribute(name = "time") @NonNull String time,
                         @ElementList(inline = true, entry = "Cube") @NonNull List<Cube> cubes) {
        this.time = time;
        this.cubes = cubes;
    }

    protected TimestampCube(Parcel in) {
        this.time = in.readString();
        this.cubes = in.createTypedArrayList(Cube.CREATOR);
    }

    @NonNull
    public String getTime() {
        return time;
    }

    @NonNull
    public List<Cube> getCubes() {
        return cubes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.time);
        dest.writeTypedList(this.cubes);
    }

}
