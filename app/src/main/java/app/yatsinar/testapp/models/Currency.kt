package app.yatsinar.testapp.models

import app.yatsinar.testapp.api.models.Cube
import app.yatsinar.testapp.api.models.CurrencyType
import com.google.auto.value.AutoValue

/**
 * Created by Roman Iatcyna on 19.02.17.
 */
@AutoValue
abstract class Currency {

    abstract val currencyType : CurrencyType

    abstract val baseCurrency : CurrencyType

    abstract val rate: Float


    companion object {

        fun create(currencyType : CurrencyType, baseCurrency : CurrencyType, rate: Float) : Currency {
            return AutoValue_Currency(currencyType, baseCurrency, rate)
        }

        fun createFrom(cube : Cube, baseCurrency: CurrencyType) : Currency {
            return AutoValue_Currency(cube.currency, baseCurrency, cube.rate.toFloat())
        }

    }

}
