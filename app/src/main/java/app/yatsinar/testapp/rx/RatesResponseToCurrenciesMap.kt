package app.yatsinar.testapp.rx

import app.yatsinar.testapp.api.models.Cube
import app.yatsinar.testapp.api.models.CurrencyType
import app.yatsinar.testapp.api.models.RatesResponse
import app.yatsinar.testapp.models.Currency
import rx.functions.Func1
import java.util.*

/**
 * Created by Roman Iatcyna on 22.02.17.
 */
class RatesResponseToCurrenciesMap(
        val baseCurrency: CurrencyType,
        val availableCurrencies: List<CurrencyType>) : Func1<RatesResponse, List<Currency>> {

    override fun call(t: RatesResponse?): List<Currency> {
        val currencies = ArrayList<Currency>()
        t?.rootCube?.timestampCube?.cubes?.forEach { cube ->
            if (availableCurrencies.contains(cube.currency)) {
                currencies.add(Currency.createFrom(cube, baseCurrency))
            }
        }
        currencies.add(Currency.create(baseCurrency, baseCurrency, 1.0f))
        return currencies
    }

}
