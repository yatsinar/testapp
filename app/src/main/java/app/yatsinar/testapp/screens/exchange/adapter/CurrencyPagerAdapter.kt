package app.yatsinar.testapp.screens.exchange.adapter

import android.view.View
import android.view.ViewGroup
import app.yatsinar.testapp.R
import app.yatsinar.testapp.api.models.CurrencyType
import app.yatsinar.testapp.models.Currency
import app.yatsinar.testapp.views.carousel.LoopingPagerAdapter
import com.mcxiaoke.koi.ext.inflate
import kotlinx.android.synthetic.main.currency_view.view.*

/**
 * Created by Roman Iatcyna on 21.02.17.
 */
class CurrencyPagerAdapter :
        LoopingPagerAdapter<CurrencyType, CurrencyPagerAdapter.ViewHolder>(true) {

    override fun onCreateViewHolder(data: CurrencyType, parent: ViewGroup): ViewHolder {
        val viewHolder = ViewHolder(parent.context.inflate(R.layout.currency_view))
        viewHolder.name.text = data.name
        return viewHolder
    }

    class ViewHolder(view: View) : LoopingPagerAdapter.ViewHolder(view) {

        val name = view.currencyName!!
        val valueEditText = view.currencyValue!!

    }

}
