package app.yatsinar.testapp.screens.exchange

import app.yatsinar.testapp.screens.ActivityScope
import dagger.Subcomponent

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
@Subcomponent(modules = arrayOf(ExchangeScreenModule::class))
@ActivityScope
interface ExchangeScreenComponent {

    fun inject(exchangeScreenActivity: ExchangeScreenActivity)

}
