package app.yatsinar.testapp.screens

import javax.inject.Scope

/**
 * Created by Roman Iatcyna on 16.02.17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope