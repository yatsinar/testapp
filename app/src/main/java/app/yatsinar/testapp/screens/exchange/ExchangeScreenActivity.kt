package app.yatsinar.testapp.screens.exchange

import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewPager
import app.yatsinar.testapp.R
import app.yatsinar.testapp.api.models.CurrencyType
import app.yatsinar.testapp.application.TestApplication
import app.yatsinar.testapp.application.base.BaseActivity
import app.yatsinar.testapp.screens.exchange.adapter.CurrencyPagerAdapter
import kotlinx.android.synthetic.main.rates_activity.*
import javax.inject.Inject

class ExchangeScreenActivity : BaseActivity<ExchangeScreenContract.Presenter, ExchangeScreenContract.View>(), ExchangeScreenContract.View {

    private val inCurrencyAdapter = CurrencyPagerAdapter()
    private val outCurrencyAdapter = CurrencyPagerAdapter()

    override fun getView() = this

    override fun createScreenComponent() {
        (application as TestApplication).getApplicationComponent()
                .getRatesActivityComponent(ExchangeScreenModule(this))
                .inject(this)
    }

    @Inject
    override fun setScreenPresenter(screenPresenter: ExchangeScreenContract.Presenter) {
        super.setScreenPresenter(screenPresenter)
    }

    private var incomingCurrency: CurrencyType? = null
    private var outgoingCurrency: CurrencyType? = null

    private var outgoingCurrencyViewHolder: CurrencyPagerAdapter.ViewHolder? = null
    private var incomingCurrencyViewHolder: CurrencyPagerAdapter.ViewHolder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.rates_activity)
        super.onCreate(savedInstanceState)

        val actionBarToolbar = ratesActivityToolbar
        setSupportActionBar(actionBarToolbar)

        incomingCurrencyViewPager.adapter = inCurrencyAdapter
        outgoingCurrencyViewPager.adapter = outCurrencyAdapter


        incomingCurrencyViewPagerIndicator.setViewPager(incomingCurrencyViewPager)
        incomingCurrencyViewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                clearText()

                val viewHolder = inCurrencyAdapter.getViewHolder(position)
                incomingCurrency = inCurrencyAdapter.items[position]

                incomingCurrencyViewHolder = viewHolder

                viewHolder?.valueEditText?.listenToFocusedTextChanges { requestOut(it) }
            }
        })

        outgoingCurrencyViewPagerIndicator.setViewPager(outgoingCurrencyViewPager)
        outgoingCurrencyViewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                clearText()

                val viewHolder = outCurrencyAdapter.getViewHolder(position)
                outgoingCurrency = outCurrencyAdapter.items[position]

                outgoingCurrencyViewHolder = viewHolder

                viewHolder?.valueEditText?.listenToFocusedTextChanges { requestIn(it) }
            }
        })
    }

    private fun clearText() {
        outgoingCurrencyViewHolder?.valueEditText?.text = null
        incomingCurrencyViewHolder?.valueEditText?.text = null
    }

    override fun showLoading(isLoading: Boolean) {
        if (isLoading) {
            progressBar.show()
        } else {
            progressBar.hide()
        }
    }

    override fun showLoadingError() {
        progressBar.hide()
        Snackbar.make(incomingCurrencyViewPager, getString(R.string.loading_error), Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.retry), { screenPresenter?.retryLoading() }).show()
    }

    private fun requestIn(value: String?) {
        val incomingCurrency = incomingCurrency ?: return
        val outgoingCurrency = outgoingCurrency ?: return

        screenPresenter?.requestIncomingCalculation(incomingCurrency, outgoingCurrency, value)
    }

    private fun requestOut(value: String?) {
        val incomingCurrency = incomingCurrency ?: return
        val outgoingCurrency = outgoingCurrency ?: return

        screenPresenter?.requestOutgoingCalculation(incomingCurrency, outgoingCurrency, value)
    }

    override fun showAvailableCurrencies(currencies: List<CurrencyType>) {
        inCurrencyAdapter.items = currencies
        outCurrencyAdapter.items = currencies


        incomingCurrencyViewPager.post({ incomingCurrencyViewPager.currentItem = 0 })
        outgoingCurrencyViewPager.post({ outgoingCurrencyViewPager.currentItem = 0 })
    }

    override fun showOutgoingResult(value: Float) {
        outgoingCurrencyViewHolder?.valueEditText?.setText(value.toString())
    }

    override fun showIncomingResult(value: Float) {
        incomingCurrencyViewHolder?.valueEditText?.setText(value.toString())
    }

    override fun notifyRatesChanged() {
        Snackbar.make(incomingCurrencyViewPager, getString(R.string.rates_were_updated), Snackbar.LENGTH_SHORT).show()
    }

}
