package app.yatsinar.testapp.screens.exchange

import java.lang.ref.WeakReference

import app.yatsinar.testapp.api.RatesApi
import dagger.Module
import dagger.Provides

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
@Module
class ExchangeScreenModule(exchangeScreenActivity: ExchangeScreenActivity) {

    private val exchangeScreenActivity: WeakReference<ExchangeScreenActivity>

    init {
        this.exchangeScreenActivity = WeakReference(exchangeScreenActivity)
    }

    @Provides
    fun provideRatesActivity(): ExchangeScreenActivity {
        return exchangeScreenActivity.get()
    }

    @Provides
    fun providePresenter(ratesApi: RatesApi): ExchangeScreenContract.Presenter {
        return ExchangeScreenPresenter(ratesApi)
    }

}
