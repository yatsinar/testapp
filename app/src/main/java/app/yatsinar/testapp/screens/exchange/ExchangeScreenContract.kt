package app.yatsinar.testapp.screens.exchange

import app.yatsinar.testapp.api.models.CurrencyType
import app.yatsinar.testapp.application.base.MvpPresenter
import app.yatsinar.testapp.application.base.MvpView

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
interface ExchangeScreenContract {

    interface View : MvpView {

        fun showAvailableCurrencies(currencies: List<CurrencyType>)

        fun showOutgoingResult(value: Float)

        fun showIncomingResult(value: Float)

        fun notifyRatesChanged()

        fun showLoading(isLoading: Boolean)

        fun showLoadingError()

    }

    interface Presenter : MvpPresenter<View> {

        fun requestIncomingCalculation(inCurrency: CurrencyType,
                                       outCurrency: CurrencyType,
                                       outgoingValue: String?)

        fun requestOutgoingCalculation(inCurrency: CurrencyType,
                                       outCurrency: CurrencyType,
                                       incomingValue: String?)

        fun retryLoading()

    }

}
