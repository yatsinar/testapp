package app.yatsinar.testapp.screens.exchange

import app.yatsinar.testapp.api.RatesApi
import app.yatsinar.testapp.api.models.CurrencyType
import app.yatsinar.testapp.application.base.BasePresenter
import app.yatsinar.testapp.models.Currency
import app.yatsinar.testapp.rx.RatesResponseToCurrenciesMap
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import rx.subscriptions.Subscriptions
import java.util.concurrent.TimeUnit

/**
 * Created by Roman Iatcyna on 12.02.17.
 */
class ExchangeScreenPresenter(
        private val ratesApi: RatesApi
) : BasePresenter<ExchangeScreenContract.View>(),
        ExchangeScreenContract.Presenter {

    companion object {
        private val baseCurrency = CurrencyType.EUR
        private val availableCurrencies = listOf<CurrencyType>(CurrencyType.USD,
                CurrencyType.GBP, CurrencyType.EUR, CurrencyType.BGN, CurrencyType.CAD)

        private val updateTime = 30L
        private val updateUnits = TimeUnit.SECONDS
    }

    private var actualCurrencies: List<Currency>? = null

    private var lastConversion: (() -> Unit)? = null

    private val retrySubject = PublishSubject.create<Void>()

    private var loadingSubscription = Subscriptions.unsubscribed()

    override fun attachView(view: ExchangeScreenContract.View) {
        super.attachView(view)
        loadingSubscription = Observable.interval(0, updateTime, updateUnits)
                .switchMap { ratesApi.rates() }
                .retryWhen({
                    notificationHandler ->
                    notificationHandler
                            .doOnNext { this.view?.showLoadingError() }
                            .flatMap { retrySubject.asObservable() }
                })
                .doOnNext { this.view?.showLoading(true) }
                .subscribeOn(Schedulers.io())
                .map(RatesResponseToCurrenciesMap(baseCurrency, availableCurrencies))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currencies ->
                    this.view?.showLoading(false)
                    if (actualCurrencies == null) {
                        this.view?.showAvailableCurrencies(availableCurrencies)
                    }

                    actualCurrencies?.forEach { oldCurrency ->
                        val newCurrency = currencies.last { it.currencyType == oldCurrency.currencyType }

                        if (newCurrency.rate != oldCurrency.rate) {
                            this.view?.notifyRatesChanged()
                            lastConversion?.invoke()
                        }
                    }
                    actualCurrencies = currencies
                }
                ) { this.view?.showLoadingError() }
    }

    override fun retryLoading() {
        retrySubject.onNext(null)
    }

    override fun requestIncomingCalculation(inCurrency: CurrencyType,
                                            outCurrency: CurrencyType,
                                            outgoingValue: String?) {
        lastConversion = { requestIncomingCalculation(inCurrency, outCurrency, outgoingValue) }

        view?.showIncomingResult(convertCurrency(outCurrency, inCurrency, outgoingValue))
    }

    override fun requestOutgoingCalculation(inCurrency: CurrencyType,
                                            outCurrency: CurrencyType,
                                            incomingValue: String?) {
        lastConversion = { requestOutgoingCalculation(inCurrency, outCurrency, incomingValue) }

        view?.showOutgoingResult(convertCurrency(inCurrency, outCurrency, incomingValue))
    }

    private fun convertCurrency(from: CurrencyType, to: CurrencyType, value: String?): Float {
        if (value == null) {
            return 0.0f
        }

        val amount: Float

        try {
            amount = value.toFloat()
        } catch (e: NumberFormatException) {
            return 0.0f
        }

        val incomingCurrencyRate = actualCurrencies?.findLast { it.currencyType == from }?.rate
        val outgoingCurrencyRate = actualCurrencies?.findLast { it.currencyType == to }?.rate

        if (incomingCurrencyRate != null && outgoingCurrencyRate != null) {
            val incomingAmountInBaseCurrency = amount / incomingCurrencyRate
            val outgoingAmount = outgoingCurrencyRate * incomingAmountInBaseCurrency

            return outgoingAmount
        } else {
            return 0.0f
        }
    }

    override fun detachView() {
        super.detachView()
        loadingSubscription.unsubscribe()
    }

}
